package com.amcastle;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class RowCount
{
	public static void main(String arg[]) throws IOException
	{
		FileInputStream fileInputStream = new FileInputStream("D:\\log_report_2017-12-02.xls");
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet sheet = workbook.getSheet("AMCastle LOG Report");
		int rowNum=sheet.getLastRowNum()+1;
		System.out.println(rowNum);
	}
}