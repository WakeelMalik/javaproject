package com.amcastle;

import java.io.BufferedReader;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LogReportCreator
{
	static Logger log = Logger.getLogger(LogReportCreator.class.getName());
	static String createNewDirectory = "\\AMCastle LOG Reports";
	static String fileToRead = "E:\\console-20181120.log";
	static String fileToBeWritten = "E:\\log_report_20181120.xlsx";
	static String workbookName = "AMCastle LOG Report";

	public static void main(String[] args) throws IOException
	{
         System.out.println("starting");   	
		log.info("STARTING STOPWATCH");
		

		try
		{
			FileInputStream inputStream = new FileInputStream(new File(fileToBeWritten));
			Workbook workbook = WorkbookFactory.create(inputStream);
			Sheet sheet = workbook.getSheetAt(0);

			Map<String, Object[]> data = new HashMap<>();
			sheet.autoSizeColumn(2);

			String filePath = fileToRead;
			File f = new File(filePath);
			BufferedReader b = new BufferedReader(new FileReader(f));
			String previousLine;
			String readLine = "";
			int count = 1;	
			int lineNumber = 0;
			while ((readLine = b.readLine()) != null)
			{
				previousLine = (lineNumber == 0) ? "" : Files.readAllLines(Paths.get(filePath)).get(lineNumber - 1);

				if (readLine.contains("| 	at") && !previousLine.contains("| 	at"))
				{
					// For getting error message
					String[] splitPreviousLine = previousLine.split("\\|");
					String errorMessage = splitPreviousLine[splitPreviousLine.length - 1];
					errorMessage = errorMessage.replaceAll("[\\[\\](){}?]", "");

					// For getting errorType
					String[] splitCurrentLine = readLine.split("\\|");
					String errorType = splitCurrentLine[splitCurrentLine.length - 1];

					// For getting Date
					previousLine = previousLine.replaceAll("INFO   \\| jvm 1    \\| main    \\| ", "");
					String date = previousLine.split(" ")[0];
					String logType = "Console";

					data.put(Integer.toString(count), new Object[]
					{ date, logType, lineNumber, errorMessage, errorType });
				}
				count++;
				lineNumber++;
				
			}
			Set<String> keyset = data.keySet();
			int rownum = sheet.getLastRowNum();
			for (String key : keyset)
			{
				Row row = sheet.createRow(rownum++);
				Object[] objArr = data.get(key);
				int cellnum = 0;
				for (Object obj : objArr)
				{
					Cell cell = row.createCell(cellnum++);
					if (obj instanceof Date)
						cell.setCellValue((Date) obj);
					else if (obj instanceof String)
						cell.setCellValue((String) obj);
					else if (obj instanceof Integer)
						cell.setCellValue((Integer) obj);
				}
			}

			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);
			inputStream.close();

			FileOutputStream outputStream = new FileOutputStream(fileToBeWritten);
			workbook.write(outputStream);
			workbook.close();
			outputStream.close();
			b.close();
		}
		catch (IOException | EncryptedDocumentException | InvalidFormatException e)
		{
			log.info(e.toString());
		}
		log.info("STOPPING STOPWATCH");
		
		Toolkit.getDefaultToolkit().beep();
		
	}
}